package com.bdf.fidel.persistence.entities;

public enum Status {
	
	ACTIVE,
	INACTIVE;

}
