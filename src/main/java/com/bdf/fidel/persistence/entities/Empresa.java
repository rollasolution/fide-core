package com.bdf.fidel.persistence.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonRootName;

@Entity
@JsonRootName("empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String razonSocial;

	private String cuit;

	@OneToOne
	@JoinColumn(name = "creador_id", referencedColumnName = "id")
	@JsonBackReference
	private Usuario creador;

	@OneToMany
	@JoinColumn(name = "empresa_id", referencedColumnName = "id")
	private List<Sucursal> sucursales;

	@OneToMany
	@JoinColumn(name = "empresa_id", referencedColumnName = "id")
	private List<Usuario> administradores;
	
	@ManyToMany(mappedBy="empresas")
	private List<Cliente> clientes;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public Usuario getCreador() {
		return creador;
	}

	public void setCreador(Usuario creador) {
		this.creador = creador;
	}

	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public List<Usuario> getAdministradores() {
		return administradores;
	}

	public void setAdministradores(List<Usuario> administradores) {
		this.administradores = administradores;
	}

	public void addAdministrador(Usuario usuario) {
		getAdministradores().add(usuario);
		
	}
	
	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

}
