package com.bdf.fidel.persistence.repositories;

import java.util.List;

import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;

public interface UsuarioRepository extends BaseRepository<Usuario, Long> {

	Usuario findByUserName(String userName);
	
	Usuario findByEmail(String email);
	
	List<Usuario> findByEmpresa(Empresa empresa);
	
}
