package com.bdf.fidel.persistence.repositories;

import com.bdf.fidel.persistence.entities.Novedad;
import com.bdf.fidel.persistence.entities.Usuario;

public interface NovedadRepository extends BaseRepository<Novedad, Long> {

	Novedad findNovedadByCreador(Usuario usuario);
	
}
