package com.bdf.fidel.persistence.repositories;

import org.springframework.stereotype.Repository;

import com.bdf.fidel.persistence.entities.Cliente;

@Repository
public interface ClienteRepository extends BaseRepository<Cliente, Long> {

	Cliente findByTwitterId(Long twitterId);
	
	Cliente findByFacebookId(Long facebookId);

    Cliente findByEmail(String email);
	
}
