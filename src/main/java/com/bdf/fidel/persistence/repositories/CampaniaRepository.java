package com.bdf.fidel.persistence.repositories;

import com.bdf.fidel.persistence.entities.Campania;

public interface CampaniaRepository extends BaseRepository<Campania, Long>{

}
