package com.bdf.fidel.persistence.repositories;

import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;

public interface EmpresaRepository extends BaseRepository<Empresa, Long> {

	Empresa findEmpresaByRazonSocial(String razonSocial);

	Empresa findEmpresaByCreador(Usuario usuario);
	
	Empresa findEmpresaByCuit(String cuit);
	
	
}
