package com.bdf.fidel.persistence.repositories;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository <T, ID extends Serializable> extends CrudRepository<T, ID> {

	T findOne(ID id);
	
}
