package com.bdf.fidel.persistence.repositories;

import com.bdf.fidel.persistence.entities.Sucursal;

public interface SucursalRepository extends BaseRepository<Sucursal, Long> {
	
	Sucursal findByNombre(String nombre);

}
