package com.bdf.fidel.persistence.converters;

import javax.persistence.AttributeConverter;

import com.bdf.fidel.enums.Sexo;

public class SexoConverter implements AttributeConverter<Sexo, String> {

	@Override
	public String convertToDatabaseColumn(Sexo attribute) {
		switch (attribute) {
			case MASCULINO: return "M";
			case FEMENINO: return "F";
			default: throw new IllegalArgumentException("Unknown sexo: " + attribute);
		}
	}

	@Override
	public Sexo convertToEntityAttribute(String dbData) {
		switch (dbData) {
			case "M": return Sexo.MASCULINO;
			case "F": return Sexo.FEMENINO;
			default: throw new IllegalArgumentException("Unknown sexo: " + dbData);
	}
	}

}
