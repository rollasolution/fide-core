package com.bdf.fidel.persistence.converters;

import javax.persistence.AttributeConverter;

import com.bdf.fidel.enums.EstadoCivil;

public class EstadoCivilConverter implements AttributeConverter<EstadoCivil, String> {

	@Override
	public String convertToDatabaseColumn(EstadoCivil attribute) {
		switch (attribute) {
			case CASADO: return "C";
			case DIVORCIADO: return "D";
			case SOLTERO: return "S";
			case VIUDO: return "V";
			
			default: throw new IllegalArgumentException("Unknown estadoCivil: " + attribute);
		}
	}

	@Override
	public EstadoCivil convertToEntityAttribute(String dbData) {
		switch (dbData) {
			case "C": return EstadoCivil.CASADO;
			case "D": return EstadoCivil.DIVORCIADO;
			case "S": return EstadoCivil.SOLTERO;
			case "V": return EstadoCivil.VIUDO;
			default: throw new IllegalArgumentException("Unknown estadoCivil: " + dbData);
		}
	}

}
