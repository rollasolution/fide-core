package com.bdf.fidel.persistence.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.bdf.fidel.persistence.entities.Status;;

@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, String> {

	@Override
	public String convertToDatabaseColumn(Status attribute) {

		switch (attribute) {
			case ACTIVE: return "active";
			case INACTIVE: return "inactive";
			default: throw new IllegalArgumentException("Unknown status: " + attribute);
		}
	}

	@Override
	public Status convertToEntityAttribute(String dbData) {

		switch (dbData) {
			case "active": return Status.ACTIVE;
			case "inactive": return Status.INACTIVE;
			default: throw new IllegalArgumentException("Unknown status: " + dbData);
		}
	}

}
