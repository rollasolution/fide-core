package com.bdf.fidel.persistence.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.bdf.fidel.persistence.entities.Perfil;

@Converter(autoApply = true)
public class PerfilConverter implements AttributeConverter<Perfil, String> {

	@Override
	public String convertToDatabaseColumn(Perfil attribute) {

		switch (attribute) {
			case ADMIN: return "admin";
			case USR: return "user";
			default: throw new IllegalArgumentException("Unknown perfil: " + attribute);
		}
	}

	@Override
	public Perfil convertToEntityAttribute(String dbData) {

		switch (dbData) {
			case "admin": return Perfil.ADMIN;
			case "user": return Perfil.USR;
			default: throw new IllegalArgumentException("Unknown perfil: " + dbData);
		}
	}

}
