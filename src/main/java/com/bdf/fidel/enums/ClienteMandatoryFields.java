package com.bdf.fidel.enums;

public enum ClienteMandatoryFields {

	NOMBRE,
	APELLIDO,
	FECHA_NAC,
	SEXO,
	LOC,
	EMAIL,
	ESTADO_CIVIL,
	HIJOS,
	TELEFONO_CELULAR,
    PASSWORD,
    CONFIRM_PASSWORD,
    PASSWORD_NOT_MACH,
    USER_EXIST
	
}
