package com.bdf.fidel.enums;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("message")
public enum Messages {

	USER_EXISTS,
	USER_NOT_EXISTS,
	COD_VERIF_INVALID,
	USER_NOT_ACTIVE,
	EMAIL_EXISTS,
	EMAIL_ERROR,
	COMPANY_NAME_EXISTS, 
	CUIT_EXISTS;
	
		
}
