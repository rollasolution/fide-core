package com.bdf.fidel.enums;

public enum EstadoCivil {

	CASADO,
	DIVORCIADO,
	SOLTERO,
	VIUDO
	
}
