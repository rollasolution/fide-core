package com.bdf.fidel.resources;


import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Perfil;

@Path("perfiles")
@Component
public class PerfilResource {

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public Response perfiles() {
		Perfil[] perfiles = Perfil.values();
		return Response.status(Response.Status.OK).entity(perfiles).build();
	}
}
