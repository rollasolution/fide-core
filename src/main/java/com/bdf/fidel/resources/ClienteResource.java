package com.bdf.fidel.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bdf.fidel.enums.ClienteMandatoryFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.bdf.fidel.dto.ClienteDTO;
import com.bdf.fidel.persistence.entities.Cliente;
import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.services.ClienteService;
import com.bdf.fidel.services.EmpresaService;

@Path("clientes")
@Component
public class ClienteResource {

	@Autowired
	ClienteService clienteService;
	
	@Autowired
	EmpresaService empresaService;

    @Autowired
    private MailSender mailSender;

	@Path("/twitterId/{twitterId}")
	@GET
	@PermitAll
	public Response findByTwitterId(@PathParam("twitterId") Long twitterId){
		
		Cliente cliente = clienteService.findClienteByTwitterId(twitterId);
		ClienteDTO dto = new ClienteDTO();
		
		if (null!=cliente){
			dto.setCliente(cliente);
		}
		
		return  Response.status(Response.Status.OK).entity(dto).build();
	}
	
	@Path("/facebookId/{facebookId}")
	@GET
	@PermitAll
	public Response findByFacebookId(@PathParam("facebookId") Long facebookId){
		
		Cliente cliente = clienteService.findClienteByFacebookId(facebookId);
		ClienteDTO dto = new ClienteDTO();
		
		if (null!=cliente){
			dto.setCliente(cliente);
		}

		return  Response.status(Response.Status.OK).entity(dto).build();
	}

	@Path("/save")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll 
	public Response saveCliente(ClienteDTO clienteDTO) {


        Cliente cliente = clienteService.findClienteByEmail(clienteDTO.getCliente().getEmail());

        if(cliente != null){
            clienteDTO.getMissingFields().clear();
            clienteDTO.getMissingFields().add(ClienteMandatoryFields.USER_EXIST);
            return Response.status(Response.Status.ACCEPTED).entity(clienteDTO).build();
        }


		if (!clienteDTO.validate().getIsValid()){
			return Response.status(Response.Status.ACCEPTED).entity(clienteDTO).build();
		}

		Empresa empresa = empresaService.findEmpresaById(Long.parseLong(clienteDTO.getEmpresaId()));
		List <Empresa> empresas = new ArrayList<>();
		empresas.add(empresa);			
		cliente = clienteDTO.getCliente();
		cliente.setFechaCreacion(new Date());		
		cliente.setEmpresas(empresas);		
		clienteService.saveCliente(cliente);
		
		return Response.status(Response.Status.ACCEPTED).entity(clienteDTO).build();

	}

    @Path("/find")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response findCliente(ClienteDTO clienteDTO) {

        Cliente cliente = clienteService.findClienteByEmail(clienteDTO.getCliente().getEmail());

        ClienteDTO dto = new ClienteDTO();

        if (null!=cliente && clienteDTO.getCliente().getPassword().equals(cliente.getPassword())){
            dto.setCliente(cliente);
        }

        return  Response.status(Response.Status.OK).entity(dto).build();

    }

    @Path("/forgotPassword")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @PermitAll
    public Response forgotPassword(ClienteDTO clienteDTO) {

        Cliente cliente = clienteService.findClienteByEmail(clienteDTO.getCliente().getEmail());

        ClienteDTO dto = new ClienteDTO();

        if (null!=cliente){

            SimpleMailMessage mailMsg = new SimpleMailMessage();
            mailMsg.setTo(cliente.getEmail());
            mailMsg.setFrom("Fidel@admin");
            mailMsg.setSubject("Forgot Password");
            mailMsg.setText("User: " + cliente.getEmail() + " Password: " + cliente.getPassword());
            mailSender.send(mailMsg);

            dto.setCliente(cliente);
        }

        return  Response.status(Response.Status.OK).entity(dto).build();

    }


}
