package com.bdf.fidel.resources;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.dto.UsuarioDTO;
import com.bdf.fidel.enums.Messages;
import com.bdf.fidel.persistence.entities.Status;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.services.UsuarioService;

@Path("usuarios")
@Component
public class UsuarioResource {

	@Autowired
	UsuarioService service;
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response listUsuarios(){
		
		List<Usuario> usuarios = service.listUsuarios();
		return Response.status(Response.Status.OK).entity(usuarios).build();
	}

	@Path("/{username}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response findUsuario(@PathParam("username") String userName){

		Usuario usuario = service.findUsuarioByUserName(userName);
		return Response.status(Response.Status.OK).entity(usuario).build();
	}

	@Path("/settings/{username}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response findUsuarioForSettings(@PathParam("username") String username){
		
		Usuario usr = service.findUsuarioByUserName(username);
		
		UsuarioDTO dto = new UsuarioDTO
							.Builder(usr.getId(), usr.getUserName(), usr.getPassword())
							.apellido(usr.getApellido())
							.nombre(usr.getNombre())
							.email(usr.getEmail())
							.build();
		
		return Response.status(Response.Status.OK).entity(dto).build();
		
	}
	
	@Path("/save")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response saveUsuario(UsuarioDTO usr){
		
		Usuario usuario = service.findUsuarioById(usr.getId());
		
		this.populateUsuario(usuario, usr);
		
		service.saveUsuario(usuario);
		
		return Response.status(Response.Status.ACCEPTED).build();
	}

	private void populateUsuario(Usuario usuario, UsuarioDTO usr) {

		usuario.setPassword(usr.getPassword());
		
		usuario.setUserName(usr.getUsername());
		
		usuario.setApellido(usr.getApellido());
		
		usuario.setNombre(usr.getNombre());
		
		usuario.setEmail(usr.getEmail());
		
	}

	@Path("/{id}")
	@DELETE
	@RolesAllowed({"ADMIN"})
	public Response deleteUsuario(@PathParam("id") Long id){

		service.deleteUsuario(id);
		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@Path("/activate/{userName}")
	@GET
	@PermitAll
	public Response activateUsuario(@PathParam("userName") String username, @QueryParam("codigoVerificacion") String codigoVerificacion){
		
		Usuario usuario = service.findUsuarioByUserName(username);
						
		if(usuario == null) 
			return Response.status(Response.Status.CONFLICT).entity(Messages.USER_NOT_EXISTS.name()).build();
		
		if(!codigoVerificacion.equals(usuario.getCodigoVerificacion()))
			return Response.status(Response.Status.CONFLICT).entity(Messages.COD_VERIF_INVALID.name()).build();
	
		usuario.setStatus(Status.ACTIVE);
		usuario.setCodigoVerificacion(null);
		service.saveUsuario(usuario);
		
		return Response.status(Response.Status.OK).build();
		
	}
	
}
