package com.bdf.fidel.resources;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

@Component
@Path("echo")
public class Echo {

	@GET
	@Path("/ping")
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public String ping(){
		return "Respondiendo desde Echo Service";
	}
	
}
