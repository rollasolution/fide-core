package com.bdf.fidel.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Campania;
import com.bdf.fidel.services.CampaniaService;

@Path("campanias")
@Component
public class CampaniaResource {

	@Autowired
	CampaniaService campaniaService;

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "ADMIN" })
	public Response listCampanias() {

		List<Campania> campanias = campaniaService.listCampanias();
		return Response.status(Response.Status.OK).entity(campanias).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ "ADMIN" })
	public Response findEmpresaByUsuarioCreador(@PathParam("id") String id) {

		Campania campania = campaniaService
				.findCampaniaById(Long.parseLong(id));

		if (campania != null)
			return Response.status(Response.Status.OK).entity(campania).build();

		return Response.status(Response.Status.NO_CONTENT).build();
	}

}
