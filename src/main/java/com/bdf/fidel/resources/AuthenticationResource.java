package com.bdf.fidel.resources;

import javax.annotation.security.PermitAll;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.enums.Messages;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.security.TokenUtils;
import com.bdf.fidel.security.UserData;
import com.bdf.fidel.services.UsuarioService;

@Path("auth")
@Component
public class AuthenticationResource {

	@Autowired
	private UsuarioService service;
	
	@Path("/login")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public Response authenticate(@HeaderParam("username") String userName, @HeaderParam("password") String password){
		
		Usuario usuario = service.findUsuarioByUserName(userName);
		
		//Valido que el usuario se encuentre activo
		if (null!=usuario && !usuario.isActive()){
			return Response.status(Response.Status.FORBIDDEN).entity(Messages.USER_NOT_ACTIVE.name()).build();
		}
		
		if (null!=usuario && usuario.getPassword().equals(password)){
			String token = TokenUtils.createToken(usuario);
			UserData userData = new UserData(usuario, token, usuario.getPerfil().toString());
		
			return Response.status(Response.Status.ACCEPTED).entity(userData).build();
		}
		
		return Response.status(Response.Status.UNAUTHORIZED).build();
		
	}
	
}
