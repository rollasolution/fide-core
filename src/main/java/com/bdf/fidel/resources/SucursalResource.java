package com.bdf.fidel.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Sucursal;
import com.bdf.fidel.services.SucursalService;

@Path("sucursales")
@Component
public class SucursalResource {

	@Autowired
	SucursalService service;

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response listSucursales() {

		List<Sucursal> sucursales = service.listSucursales();
		return Response.status(Response.Status.OK).entity(sucursales).build();
	}

	@Path("/{nombre}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response findSucursal(@PathParam("nombre") String nombre) {

		Sucursal sucursal = service.findSucursalByNombre(nombre);
		return Response.status(Response.Status.OK).entity(sucursal).build();
	}

	@Path("/save")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response saveSucursal(Sucursal sucursal) {

		service.saveSucursal(sucursal);
		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@Path("/{id}")
	@DELETE
	@RolesAllowed({"ADMIN"})
	public Response deleteSucursal(@PathParam("id") Long id){

		service.deleteSucursal(id);
		return Response.status(Response.Status.ACCEPTED).build();
	}

}
