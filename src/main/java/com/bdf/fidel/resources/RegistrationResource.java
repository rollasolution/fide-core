package com.bdf.fidel.resources;

import java.util.UUID;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bdf.fidel.dto.UserEmpresaDTO;
import com.bdf.fidel.enums.Messages;
import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.services.EmpresaService;
import com.bdf.fidel.services.UsuarioService;

@Path("registration")
@Component
public class RegistrationResource {

	@Autowired
	private UsuarioService service;
	
	@Autowired
	private EmpresaService empresaService;
	
	@Autowired
	private MailSender mailSender;
	
	@Path("/signup")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@PermitAll
	@Transactional
	public Response signup(UserEmpresaDTO userEmpresaDTO){

		//Valido que no exista un usuario registrado con el mismo userName y eMail.
		Usuario usrByUserName = service.findUsuarioByUserName(userEmpresaDTO.getUserName());
		Usuario usrByEmail = service.findUsuarioByEmail(userEmpresaDTO.getEmail());
		
		String message = "";
		if (null!=usrByUserName){
			message = Messages.USER_EXISTS.name();
			return Response.status(Response.Status.CONFLICT).entity(message).build();
		}
		
		if (null!=usrByEmail){
			message = Messages.EMAIL_EXISTS.name();
			return Response.status(Response.Status.CONFLICT).entity(message).build();
		}
		
		String codigoVerificacion = UUID.randomUUID().toString();
		userEmpresaDTO.setCodigoVerificacion(codigoVerificacion);
		
		service.saveUsuario(userEmpresaDTO.getUsuario());
		
		if(userEmpresaDTO.getUsuario().isUSR())
			this.asociarEmpresa(userEmpresaDTO);
			
		String url =  "http://localhost:8080/activate?userName=" + userEmpresaDTO.getUserName() +  "&codigoVerificacion=" + codigoVerificacion;
		
		try{
			this.sendVerificationEmail(userEmpresaDTO.getUsuario(), url);
			
		}catch(MailException e){
			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(Messages.EMAIL_ERROR.name()).build();
		}
		
		return Response.status(Response.Status.OK).build();
		
	}

	private void asociarEmpresa(UserEmpresaDTO userEmpresaDTO) {
		Empresa empresa = empresaService.findEmpresaById(Long.parseLong(userEmpresaDTO.getIdEmpresa()));
		empresa.addAdministrador(userEmpresaDTO.getUsuario());
		empresaService.saveEmpresa(empresa);
	}

	/**
	 * 
	 * @param usuario
	 * @param url
	 * @throws MailException
	 */
	private void sendVerificationEmail(Usuario usuario, String url) throws MailException {
		
		SimpleMailMessage mailMsg = new SimpleMailMessage();
		mailMsg.setTo(usuario.getEmail());
		mailMsg.setFrom("Fidel@admin");
		mailMsg.setSubject("Verificación de registro de usuario");
		mailMsg.setText(url);
		
		mailSender.send(mailMsg);
	
	}
	
}
