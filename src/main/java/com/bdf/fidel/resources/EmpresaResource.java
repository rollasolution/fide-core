package com.bdf.fidel.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.dto.EmpresaDTO;
import com.bdf.fidel.enums.Messages;
import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.services.EmpresaService;
import com.bdf.fidel.services.UsuarioService;

@Path("empresas")
@Component
public class EmpresaResource {

	@Autowired
	EmpresaService empresaService;

	@Autowired
	UsuarioService usuarioService;
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response listEmpresas() {

		List<Empresa> empresas = empresaService.listEmpresas();
		return Response.status(Response.Status.OK).entity(empresas).build();
	}

	@Path("/{razonSocial}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response findEmpresa(@PathParam("razonSocial") String razonSocial) {

		Empresa empresa = empresaService.findEmpresaByRazonSocial(razonSocial);
		return Response.status(Response.Status.OK).entity(empresa).build();
	}

	@Path("/usuarios/{usuarioId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response findEmpresaByUsuarioCreador(@PathParam("usuarioId") String usuarioId) {

		Usuario usuario = usuarioService.findUsuarioById(Long.parseLong(usuarioId));
		Empresa empresa = empresaService.findEmpresaByCreador(usuario);

		if (empresa!=null)
			return Response.status(Response.Status.OK).entity(empresa).build();
		
		
		return Response.status(Response.Status.NO_CONTENT).build();
	}
	
	@Path("/save")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response saveEmpresa(EmpresaDTO empresaDTO) {

		Usuario usuario = usuarioService.findUsuarioById(Long.parseLong(empresaDTO.getIdUsuario()));

		Empresa empresa = empresaDTO.getEmpresa();	
		
		if(empresaService.findEmpresaByRazonSocial(empresa.getRazonSocial()) != null)
			return Response.status(Response.Status.CONFLICT).entity(Messages.COMPANY_NAME_EXISTS.name()).build();
		
		if(empresaService.findEmpresaByCuit(empresa.getCuit()) != null)
			return Response.status(Response.Status.CONFLICT).entity(Messages.CUIT_EXISTS.name()).build();
		
		empresa.setCreador(usuario); 
		
		empresaService.saveEmpresa(empresa);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@Path("/{id}")
	@DELETE
	@RolesAllowed({"ADMIN"})
	public Response deleteEmpresa(@PathParam("id") Long id){

		empresaService.deleteEmpresa(id);
		return Response.status(Response.Status.ACCEPTED).build();
	}

}
