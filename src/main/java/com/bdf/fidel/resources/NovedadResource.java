package com.bdf.fidel.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.dto.NovedadDTO;
import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Novedad;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.services.EmpresaService;
import com.bdf.fidel.services.NovedadService;
import com.bdf.fidel.services.UsuarioService;

@Path("novedades")
@Component
public class NovedadResource {
	
	@Autowired
	NovedadService novedadservice;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	EmpresaService empresaService;
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN"})
	public Response listUsuarios(){
		
		List<Novedad> novedades = novedadservice.listNovedades();
		return Response.status(Response.Status.OK).entity(novedades).build();
	}
	
	
	@Path("/save")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"ADMIN", "USR"})
	public Response saveNovedad(NovedadDTO novedadDTO){
		
		Usuario usuario = usuarioService.findUsuarioById(Long.parseLong(novedadDTO.getIdUsuario()));
		Empresa empresa = empresaService.findEmpresaByCreador(usuario);
		Novedad novedad = novedadDTO.getNovedad();
		novedad.setIdEmpresa(empresa.getId());
		novedad.setCreador(usuario);
		
		novedadservice.saveNovedad(novedad);
		
		return Response.status(Response.Status.ACCEPTED).build();
	}
	
}
