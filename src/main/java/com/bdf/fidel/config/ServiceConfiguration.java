package com.bdf.fidel.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@ComponentScan("com.bdf.fidel")
public class ServiceConfiguration {
	
	@Bean
	public JavaMailSender javaMailSender(){
	    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
	    javaMailSender.setHost( "smtp.gmail.com" );
	    javaMailSender.setDefaultEncoding("UTF-8");
	    
	    //TODO: Setear usr y pass....
	    javaMailSender.setUsername("fidelmailsender@gmail.com");
	    javaMailSender.setPassword("fidelsender");
	    
	    //SSL Gmail PORT
	    javaMailSender.setPort(587);
	    javaMailSender.setProtocol("smtp");
	    
	    Properties properties = new Properties();
		properties.put("mail.smtp.auth", Boolean.TRUE);
		properties.put("mail.smtp.starttls.enable", Boolean.TRUE);
		
	    javaMailSender.setJavaMailProperties(properties);
	    
	    return javaMailSender;
	}
	
}
