package com.bdf.fidel.config;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;


public class WebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) {
        
    	this.createRootContext(servletContext);
        
    	this.configureJerseyServlet(servletContext);

        this.configureOpenEntityManagerInViewFilter(servletContext);
    }

    
    private WebApplicationContext createRootContext(ServletContext servletContext) {
	
    	AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
	    rootContext.register(JPAConfiguration.class, ServiceConfiguration.class);
	
	    rootContext.refresh();
	
	    servletContext.addListener(new ContextLoaderListener(rootContext));
	    servletContext.setInitParameter("defaultHtmlEscape", "true");
	    rootContext.scan("com.bdf.fidel");
	    
	    return rootContext;
    
    }
    
    private void configureJerseyServlet(ServletContext servletContext) {
    
    	SpringServlet jerseyServlet = new SpringServlet();

    	ServletRegistration.Dynamic appServlet = servletContext.addServlet("JerseyServlet", jerseyServlet);

    	appServlet.setInitParameter("org.glassfish.jersey.config.property.packages", "com.fasterxml.jackson.jaxrs.json");
        appServlet.setInitParameter("com.sun.jersey.spi.container.ResourceFilters", "com.bdf.fidel.filters.ResourceFilterFactory");
        
        appServlet.setInitParameter("com.sun.jersey.spi.container.ContainerResponseFilters", "com.bdf.fidel.filters.CORSResponseFilter");
        
        appServlet.setLoadOnStartup(1);

        appServlet.addMapping("/rest/*");
        
    }

    private void configureOpenEntityManagerInViewFilter(ServletContext servletContext) {
        
    	FilterRegistration.Dynamic openEntityManagerInView = servletContext.addFilter("OpenEntityManagerInViewFilter",
	    new OpenEntityManagerInViewFilter());
	    
    	openEntityManagerInView.setInitParameter("entityManagerFactoryBeanName", "entityManagerFactory");
    	
    	openEntityManagerInView.addMappingForUrlPatterns(null, true, "/*");
	}
    
}
