package com.bdf.fidel.dto;

import java.io.Serializable;

import com.bdf.fidel.persistence.entities.Novedad;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("novedadDTO")
public class NovedadDTO implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 642539191098424205L;
	
	private String idUsuario;
	private Novedad novedad;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Novedad getNovedad() {
		return novedad;
	}
	public void setNovedad(Novedad novedad) {
		this.novedad = novedad;
	}
	

	
	
	
}
