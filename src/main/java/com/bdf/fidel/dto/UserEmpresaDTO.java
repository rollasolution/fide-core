package com.bdf.fidel.dto;

import java.io.Serializable;

import com.bdf.fidel.persistence.entities.Usuario;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("userEmpresaDTO")
public class UserEmpresaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5078538817546602260L;

	private String idEmpresa;
	private Usuario usuario;
	
	public String getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getUserName() {
		return getUsuario().getUserName();
	}
	public String getEmail() {
		return getUsuario().getEmail();
	}
	public void setCodigoVerificacion(String codigoVerificacion) {
		getUsuario().setCodigoVerificacion(codigoVerificacion);
	}

	
	
}