package com.bdf.fidel.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * 
 * DTO utilizado para cambiar los settings de un usuario (ya que la clase de dominio 
 * no serializa el password e incluye relaciones que no aplican a settings) 
 *
 */
@JsonRootName("usuarioDTO")
public class UsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2310549681578521583L;

	private Long id;

	private String username;

	private String password;

	private String apellido;

	private String nombre;

	private String email;

	public UsuarioDTO(){
	}
	
	public static class Builder{
		
		private Long id;
		private String username;
		private String password;
		private String apellido;
		private String nombre;
		private String email;
		
		public Builder(Long id, String username, String password){
			this.id = id;
			this.username = username;
			this.password = password;
		}
		
		public Builder apellido(String apellido){
			this.apellido = apellido;
			return this;
		}
		
		public Builder nombre(String nombre){
			this.nombre = nombre;
			return this;
		}
		
		public Builder email(String email){
			this.email = email;
			return this;
		}
		
		public UsuarioDTO build(){
			return new UsuarioDTO(this);
		}
	}
	
	public UsuarioDTO(Builder builder) {
		this.id = builder.id;
		this.username = builder.username;
		this.password = builder.password;
		this.apellido = builder.apellido;
		this.nombre = builder.nombre;
		this.email = builder.email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
