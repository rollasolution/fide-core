package com.bdf.fidel.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.routines.EmailValidator;

import com.bdf.fidel.enums.ClienteMandatoryFields;
import com.bdf.fidel.persistence.entities.Cliente;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("clienteDTO")
public class ClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -468650863362532324L;
	
	private String empresaId;
	private Cliente cliente;
	
	private Boolean isValid = Boolean.FALSE;
	
	private List<ClienteMandatoryFields> missingFields = new ArrayList<ClienteMandatoryFields>();

	public ClienteDTO validate(){
		
		if (cliente.getNombre() == null || cliente.getNombre().isEmpty()) {
			missingFields.add(ClienteMandatoryFields.NOMBRE);
		}
		
		if (cliente.getApellido() == null || cliente.getApellido().isEmpty()) {
			missingFields.add(ClienteMandatoryFields.APELLIDO);
		}
		
		if (cliente.getFechaNacimiento() == null) {
			missingFields.add(ClienteMandatoryFields.FECHA_NAC);
		}
		
		if (cliente.getSexo() == null) {
			missingFields.add(ClienteMandatoryFields.SEXO);
		}
		
		if (cliente.getLocalidad() == null || cliente.getLocalidad().isEmpty()) {
			missingFields.add(ClienteMandatoryFields.LOC);
		}
		
		EmailValidator emv = EmailValidator.getInstance();
		
		if (cliente.getEmail() == null || cliente.getEmail().isEmpty() || !emv.isValid(cliente.getEmail()) ) {
			missingFields.add(ClienteMandatoryFields.EMAIL);
		}

		if (cliente.getEstadoCivil() == null) {
			missingFields.add(ClienteMandatoryFields.ESTADO_CIVIL);
		}
		
		if (cliente.getCantidadHijos() == null || cliente.getCantidadHijos() < 0) {
			missingFields.add(ClienteMandatoryFields.HIJOS);
		}

		if (cliente.getTelefonoCelular() == null || cliente.getTelefonoCelular().isEmpty()) {
			missingFields.add(ClienteMandatoryFields.TELEFONO_CELULAR);
		}
        if (cliente.getIsEmailSingup()){

            if (cliente.getPassword() == null || cliente.getPassword().isEmpty()) {
                missingFields.add(ClienteMandatoryFields.PASSWORD);
            }

            if (cliente.getConfirmPassword() == null || cliente.getConfirmPassword().isEmpty()) {
                missingFields.add(ClienteMandatoryFields.CONFIRM_PASSWORD);
            }

            if (cliente.getConfirmPassword() != null && !cliente.getConfirmPassword().isEmpty() &&
                    cliente.getPassword() != null && !cliente.getPassword().isEmpty()
                    && !cliente.getPassword().equals(cliente.getConfirmPassword()))  {
                missingFields.add(ClienteMandatoryFields.PASSWORD_NOT_MACH);
            }
        }

		
		if (missingFields.size()==0)
			this.isValid = Boolean.TRUE;
			
		return this;
		
	}
	
	public String getEmpresaId() {
		return empresaId;
	}
	
	public void setEmpresaId(String empresaId) {
		this.empresaId = empresaId;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Boolean getIsValid() {
		return isValid;
	}
	
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	public List<ClienteMandatoryFields> getMissingFields() {
		return missingFields;
	}

	public void setMissingFields(List<ClienteMandatoryFields> missingFields) {
		this.missingFields = missingFields;
	}
	
}
