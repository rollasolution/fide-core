package com.bdf.fidel.dto;

import java.io.Serializable;

import com.bdf.fidel.persistence.entities.Empresa;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("empresaDTO")
public class EmpresaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5078538817546602260L;

	private String idUsuario;
	private Empresa empresa;
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
	
}
