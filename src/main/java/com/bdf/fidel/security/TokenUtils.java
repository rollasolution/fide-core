package com.bdf.fidel.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bouncycastle.util.encoders.Hex;

import com.bdf.fidel.persistence.entities.Usuario;

public class TokenUtils {

		public static final String MAGIC_KEY = "obfuscate";

		public static String createToken(Usuario usuario) {
			
			/* Expires in one hour */
			long expires = System.currentTimeMillis() + 1000L * 60 * 60;

			StringBuilder tokenBuilder = new StringBuilder();
			tokenBuilder.append(usuario.getUserName());
			tokenBuilder.append(":");
			tokenBuilder.append(expires);
			tokenBuilder.append(":");
			tokenBuilder.append(TokenUtils.computeSignature(usuario.getUserName(), usuario.getPassword(), expires));

			return tokenBuilder.toString();
		}

		public static String computeSignature(String userName, String password, long expires) {

			StringBuilder signatureBuilder = new StringBuilder();
			signatureBuilder.append(userName);
			signatureBuilder.append(":");
			signatureBuilder.append(expires);
			signatureBuilder.append(":");
			signatureBuilder.append(password);
			signatureBuilder.append(":");
			signatureBuilder.append(TokenUtils.MAGIC_KEY);

			MessageDigest digest;
			try {
				digest = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				throw new IllegalStateException("No MD5 algorithm available!");
			}

			return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
		}


		public static String getUserNameFromToken(String authToken) {

			if (null == authToken) {
				return null;
			}

			String[] parts = authToken.split(":");
			return parts[0];
		}


		public static boolean validateToken(String authToken, Usuario usuario) {
			
			String[] parts = authToken.split(":");
			long expires = Long.parseLong(parts[1]);
			String signature = parts[2];

			if (expires < System.currentTimeMillis()) {
				return false;
			}

			return signature.equals(TokenUtils.computeSignature(usuario.getUserName(), usuario.getPassword(), expires));
			
		}
	
}
