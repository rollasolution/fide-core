package com.bdf.fidel.security;

import java.security.Principal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.bdf.fidel.persistence.entities.Usuario;

public class FidelSecurityContext implements SecurityContext {

	private final Usuario user;
	private final Boolean authorized;

	public FidelSecurityContext(Usuario user, Boolean authorized){
		this.user = user;
		this.authorized = authorized;
	}
	
	@Override
	public Principal getUserPrincipal() {
		return user;
	}

	@Override
	public boolean isUserInRole(String role) {
		 if (!authorized) {
	            // Forbidden
	            Response denied = Response.status(Response.Status.FORBIDDEN).entity("Permission Denied").build();
	            throw new WebApplicationException(denied);
	        }
	 
	        try {
	            //this user has this role?
	            return user.getPerfil().toString().equals(role);
	        } catch (Exception e) {
	        }
	         
	        return false;
	}

	@Override
	public boolean isSecure() {
		return this.authorized;
	}

	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	public Boolean getAuthorized() {
		return authorized;
	}

}
