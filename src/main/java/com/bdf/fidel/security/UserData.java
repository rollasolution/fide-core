package com.bdf.fidel.security;

import com.bdf.fidel.persistence.entities.Usuario;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("user-data")
public class UserData {

	private Usuario usuario;
	
	private String perfil;
	private String token;

	public UserData(){
	}
	
	public UserData(Usuario usuario, String token, String perfil) {
		this.usuario = usuario;
		this.token = token;
		this.perfil = perfil;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

}
