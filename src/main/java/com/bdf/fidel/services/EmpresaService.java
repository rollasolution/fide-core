package com.bdf.fidel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.persistence.repositories.EmpresaRepository;
import com.google.common.collect.Lists;

@Component
public class EmpresaService {

	@Autowired
	private EmpresaRepository repository;

	public Empresa findEmpresaByRazonSocial(String razonSocial) {
		return repository.findEmpresaByRazonSocial(razonSocial);
	}

	public Empresa findEmpresaByCreador(Usuario usuario) {
		return repository.findEmpresaByCreador(usuario);
	}
	
	public Empresa findEmpresaByCuit(String cuit) {
		return repository.findEmpresaByCuit(cuit);
	}
	
	public Empresa findEmpresaById(Long id){
		return repository.findOne(id);
	}

	public void saveEmpresa(Empresa empresa) {
		repository.save(empresa);
	}

	public List<Empresa> listEmpresas() {
		return Lists.newArrayList(repository.findAll());
	}

	public void deleteEmpresa(long id) {
		repository.delete(id);
	}


}
