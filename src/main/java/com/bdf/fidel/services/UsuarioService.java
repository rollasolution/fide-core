package com.bdf.fidel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Empresa;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.persistence.repositories.EmpresaRepository;
import com.bdf.fidel.persistence.repositories.UsuarioRepository;
import com.google.common.collect.Lists;

@Component
public class UsuarioService {

	@Autowired
	private UsuarioRepository repository;
	
	private EmpresaRepository empresaRepository;
	
	public Usuario findUsuarioByUserName(String userName){
		return repository.findByUserName(userName);
	}
	
	public Usuario findUsuarioByEmail(String email){
		return repository.findByEmail(email);
	}
	
	public Usuario findUsuarioById(Long id){
		return repository.findOne(id);
	}
	
	public void saveUsuario(Usuario usuario){
		repository.save(usuario);
	}

	public List<Usuario> listUsuariosByEmpresa(Long empresaId) {
		
		Empresa empresa = empresaRepository.findOne(empresaId);
		
		return Lists.newArrayList(repository.findByEmpresa(empresa));

	}
	
	public List<Usuario> listUsuarios() {
		return Lists.newArrayList(repository.findAll());
	}

	public void deleteUsuario(Long id) {
		repository.delete(id);
	}
	
}
