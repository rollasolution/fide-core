package com.bdf.fidel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Sucursal;
import com.bdf.fidel.persistence.repositories.SucursalRepository;
import com.google.common.collect.Lists;

@Component
public class SucursalService {

	@Autowired
	private SucursalRepository repository; 
	
	public Sucursal findSucursalByNombre(String nombre) {
		return repository.findByNombre(nombre);
	}
	
	public void saveSucursal(Sucursal sucursal) {
		repository.save(sucursal);
	}
	
	public List<Sucursal> listSucursales() {
		return Lists.newArrayList(repository.findAll());
	}
	
	public void deleteSucursal(Long id) {
		repository.delete(id);
	}
}
