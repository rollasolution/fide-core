package com.bdf.fidel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Novedad;
import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.persistence.repositories.NovedadRepository;
import com.google.common.collect.Lists;

@Component
public class NovedadService {

	@Autowired
	private NovedadRepository repository;

	public Novedad findNovedadByCreador(Usuario usuario){
		return this.repository.findNovedadByCreador(usuario);
	}
	
	public void saveNovedad (Novedad novedad){
		repository.save(novedad);
	}

	public List<Novedad> listNovedades() {

		return Lists.newArrayList(repository.findAll());
	}

}
