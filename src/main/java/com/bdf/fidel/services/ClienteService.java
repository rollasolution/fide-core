package com.bdf.fidel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Cliente;
import com.bdf.fidel.persistence.repositories.ClienteRepository;

@Component
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	public Cliente findClienteByTwitterId(Long twitterId){
		return repository.findByTwitterId(twitterId);
	}
	
	public Cliente findClienteByFacebookId(Long facebookId){
		return repository.findByFacebookId(facebookId);
	}
	
	public void saveCliente(Cliente cliente){
		repository.save(cliente);
	}

    public Cliente findClienteByEmail (String email) {
        return repository.findByEmail(email);
    }
	
}
