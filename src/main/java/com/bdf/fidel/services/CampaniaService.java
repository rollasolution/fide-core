package com.bdf.fidel.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Campania;
import com.bdf.fidel.persistence.repositories.CampaniaRepository;
import com.google.common.collect.Lists;


@Component
public class CampaniaService {
	
	@Autowired
	private CampaniaRepository repository;
	
	public Campania findCampaniaById(Long id){
		return repository.findOne(id);
	}
	
	public void saveCampania(Campania campania) {
		repository.save(campania);
	}

	public List<Campania> listCampanias() {
		return Lists.newArrayList(repository.findAll());
	}

	public void deleteCampania(long id) {
		repository.delete(id);
	}

}
