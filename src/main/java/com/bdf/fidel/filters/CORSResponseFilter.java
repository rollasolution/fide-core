package com.bdf.fidel.filters;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

@Component   // let spring manage the lifecycle
@Provider    // register as jersey's provider
public class CORSResponseFilter implements ContainerResponseFilter {

	@Override
	public ContainerResponse filter(ContainerRequest request,
			ContainerResponse response) {

		MultivaluedMap<String, Object> headers = response.getHttpHeaders();
		 
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "HEAD, GET, POST, DELETE, PUT, OPTIONS");            
		headers.add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, username, password, X-Auth-Token");
        
        return response;
        
	}

}
