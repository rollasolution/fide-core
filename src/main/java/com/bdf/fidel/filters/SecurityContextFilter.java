package com.bdf.fidel.filters;

import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdf.fidel.persistence.entities.Usuario;
import com.bdf.fidel.security.FidelSecurityContext;
import com.bdf.fidel.security.TokenUtils;
import com.bdf.fidel.services.UsuarioService;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

@Component   // let spring manage the lifecycle
@Provider    // register as jersey's provider
public class SecurityContextFilter implements ResourceFilter, ContainerRequestFilter {

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public ContainerRequest filter(ContainerRequest request) {

		//Get authentication token from request header
        final String authToken = request.getHeaderValue("X-Auth-Token");
 
        String userName = TokenUtils.getUserNameFromToken(authToken);
        
        Usuario usuario = usuarioService.findUsuarioByUserName(userName);
        
        if (usuario!=null){
        	Boolean isValidToken = TokenUtils.validateToken(authToken, usuario);
        	
        	//Set security context
        	request.setSecurityContext(new FidelSecurityContext(usuario, isValidToken));
        }

        return request;
        
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return this;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
