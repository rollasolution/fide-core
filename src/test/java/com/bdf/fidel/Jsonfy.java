package com.bdf.fidel;

import org.junit.Test;

import com.bdf.fidel.persistence.entities.Usuario;
import com.google.gson.Gson;

public class Jsonfy {

	@Test
	public void jsonFy(){
		
		Usuario u = new Usuario();
		
		u.setId(1L);
		u.setApellido("user1");
		u.setNombre("");
		u.setEmail("");
		u.setPassword("");
		u.setUserName("");
		
		Gson gson = new Gson();
		
		System.out.println(gson.toJson(u));
		
		
	}
	
	
}
